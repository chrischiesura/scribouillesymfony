<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomLieu;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionLieu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeLieu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgLieu;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieUnivers::class, inversedBy="lieus")
     */
    private $categorieUnivers;

    /**
     * @ORM\ManyToMany(targetEntity=Personnage::class, inversedBy="lieus")
     */
    private $LieuPersonnage;

    public function __construct()
    {
        $this->LieuPersonnage = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomLieu(): ?string
    {
        return $this->nomLieu;
    }

    public function setNomLieu(?string $nomLieu): self
    {
        $this->nomLieu = $nomLieu;

        return $this;
    }

    public function getDescriptionLieu(): ?string
    {
        return $this->descriptionLieu;
    }

    public function setDescriptionLieu(?string $descriptionLieu): self
    {
        $this->descriptionLieu = $descriptionLieu;

        return $this;
    }

    public function getTypeLieu(): ?string
    {
        return $this->typeLieu;
    }

    public function setTypeLieu(?string $typeLieu): self
    {
        $this->typeLieu = $typeLieu;

        return $this;
    }

    public function getImgLieu(): ?string
    {
        return $this->imgLieu;
    }

    public function setImgLieu(?string $imgLieu): self
    {
        $this->imgLieu = $imgLieu;

        return $this;
    }

    public function getCategorieUnivers(): ?CategorieUnivers
    {
        return $this->categorieUnivers;
    }

    public function setCategorieUnivers(?CategorieUnivers $categorieUnivers): self
    {
        $this->categorieUnivers = $categorieUnivers;

        return $this;
    }

    /**
     * @return Collection|Personnage[]
     */
    public function getLieuPersonnage(): Collection
    {
        return $this->LieuPersonnage;
    }

    public function addLieuPersonnage(Personnage $lieuPersonnage): self
    {
        if (!$this->LieuPersonnage->contains($lieuPersonnage)) {
            $this->LieuPersonnage[] = $lieuPersonnage;
        }

        return $this;
    }

    public function removeLieuPersonnage(Personnage $lieuPersonnage): self
    {
        $this->LieuPersonnage->removeElement($lieuPersonnage);

        return $this;
    }
}

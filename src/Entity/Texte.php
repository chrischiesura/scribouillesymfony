<?php

namespace App\Entity;

use App\Repository\TexteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TexteRepository::class)
 */
class Texte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreTexte;

    /**
     * @ORM\Column(type="text")
     */
    private $texte;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $creationTexte;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $modificationTexte;

    /**
     * @ORM\ManyToOne(targetEntity=Roman::class, inversedBy="textes")
     */
    private $roman;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreTexte(): ?string
    {
        return $this->titreTexte;
    }

    public function setTitreTexte(?string $titreTexte): self
    {
        $this->titreTexte = $titreTexte;

        return $this;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getCreationTexte(): ?\DateTimeInterface
    {
        return $this->creationTexte;
    }

    public function setCreationTexte(?\DateTimeInterface $creationTexte): self
    {
        $this->creationTexte = $creationTexte;

        return $this;
    }

    public function getModificationTexte(): ?\DateTimeInterface
    {
        return $this->modificationTexte;
    }

    public function setModificationTexte(?\DateTimeInterface $modificationTexte): self
    {
        $this->modificationTexte = $modificationTexte;

        return $this;
    }

    public function getRoman(): ?Roman
    {
        return $this->roman;
    }

    public function setRoman(?Roman $roman): self
    {
        $this->roman = $roman;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\BestiaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BestiaireRepository::class)
 */
class Bestiaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenuBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $forceBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $faiblesseBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $apparenceBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $alimentaireBestiaire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $particulariteBestiaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgBestiaire;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieUnivers::class, inversedBy="bestiaires")
     */
    private $categorieUnivers;

    /**
     * @ORM\ManyToMany(targetEntity=Pouvoir::class, inversedBy="bestiaires")
     */
    private $PouvoirBestiaire;

    /**
     * @ORM\ManyToMany(targetEntity=Personnage::class, inversedBy="bestiaires")
     */
    private $BestiairePersonnage;

    public function __construct()
    {
        $this->PouvoirBestiaire = new ArrayCollection();
        $this->BestiairePersonnage = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomBestiaire(): ?string
    {
        return $this->nomBestiaire;
    }

    public function setNomBestiaire(string $nomBestiaire): self
    {
        $this->nomBestiaire = $nomBestiaire;

        return $this;
    }

    public function getContenuBestiaire(): ?string
    {
        return $this->contenuBestiaire;
    }

    public function setContenuBestiaire(?string $contenuBestiaire): self
    {
        $this->contenuBestiaire = $contenuBestiaire;

        return $this;
    }

    public function getForceBestiaire(): ?string
    {
        return $this->forceBestiaire;
    }

    public function setForceBestiaire(?string $forceBestiaire): self
    {
        $this->forceBestiaire = $forceBestiaire;

        return $this;
    }

    public function getFaiblesseBestiaire(): ?string
    {
        return $this->faiblesseBestiaire;
    }

    public function setFaiblesseBestiaire(?string $faiblesseBestiaire): self
    {
        $this->faiblesseBestiaire = $faiblesseBestiaire;

        return $this;
    }

    public function getApparenceBestiaire(): ?string
    {
        return $this->apparenceBestiaire;
    }

    public function setApparenceBestiaire(?string $apparenceBestiaire): self
    {
        $this->apparenceBestiaire = $apparenceBestiaire;

        return $this;
    }

    public function getAlimentaireBestiaire(): ?string
    {
        return $this->alimentaireBestiaire;
    }

    public function setAlimentaireBestiaire(?string $alimentaireBestiaire): self
    {
        $this->alimentaireBestiaire = $alimentaireBestiaire;

        return $this;
    }

    public function getParticulariteBestiaire(): ?string
    {
        return $this->particulariteBestiaire;
    }

    public function setParticulariteBestiaire(?string $particulariteBestiaire): self
    {
        $this->particulariteBestiaire = $particulariteBestiaire;

        return $this;
    }

    public function getImgBestiaire(): ?string
    {
        return $this->imgBestiaire;
    }

    public function setImgBestiaire(?string $imgBestiaire): self
    {
        $this->imgBestiaire = $imgBestiaire;

        return $this;
    }

    public function getCategorieUnivers(): ?CategorieUnivers
    {
        return $this->categorieUnivers;
    }

    public function setCategorieUnivers(?CategorieUnivers $categorieUnivers): self
    {
        $this->categorieUnivers = $categorieUnivers;

        return $this;
    }

    /**
     * @return Collection|Pouvoir[]
     */
    public function getPouvoirBestiaire(): Collection
    {
        return $this->PouvoirBestiaire;
    }

    public function addPouvoirBestiaire(Pouvoir $pouvoirBestiaire): self
    {
        if (!$this->PouvoirBestiaire->contains($pouvoirBestiaire)) {
            $this->PouvoirBestiaire[] = $pouvoirBestiaire;
        }

        return $this;
    }

    public function removePouvoirBestiaire(Pouvoir $pouvoirBestiaire): self
    {
        $this->PouvoirBestiaire->removeElement($pouvoirBestiaire);

        return $this;
    }

    /**
     * @return Collection|Personnage[]
     */
    public function getBestiairePersonnage(): Collection
    {
        return $this->BestiairePersonnage;
    }

    public function addBestiairePersonnage(Personnage $bestiairePersonnage): self
    {
        if (!$this->BestiairePersonnage->contains($bestiairePersonnage)) {
            $this->BestiairePersonnage[] = $bestiairePersonnage;
        }

        return $this;
    }

    public function removeBestiairePersonnage(Personnage $bestiairePersonnage): self
    {
        $this->BestiairePersonnage->removeElement($bestiairePersonnage);

        return $this;
    }
}

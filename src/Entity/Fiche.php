<?php

namespace App\Entity;

use App\Repository\FicheRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FicheRepository::class)
 */
class Fiche
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreFiche;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenuFiche;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgFiche;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieUnivers::class, inversedBy="fiches")
     */
    private $categorieUnivers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreFiche(): ?string
    {
        return $this->titreFiche;
    }

    public function setTitreFiche(string $titreFiche): self
    {
        $this->titreFiche = $titreFiche;

        return $this;
    }

    public function getContenuFiche(): ?string
    {
        return $this->contenuFiche;
    }

    public function setContenuFiche(?string $contenuFiche): self
    {
        $this->contenuFiche = $contenuFiche;

        return $this;
    }

    public function getImgFiche(): ?string
    {
        return $this->imgFiche;
    }

    public function setImgFiche(?string $imgFiche): self
    {
        $this->imgFiche = $imgFiche;

        return $this;
    }

    public function getCategorieUnivers(): ?CategorieUnivers
    {
        return $this->categorieUnivers;
    }

    public function setCategorieUnivers(?CategorieUnivers $categorieUnivers): self
    {
        $this->categorieUnivers = $categorieUnivers;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\UniversRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UniversRepository::class)
 */
class Univers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomUnivers;

    /**
     * @ORM\OneToMany(targetEntity=Roman::class, mappedBy="univers")
     */
    private $UniversRoman;

    /**
     * @ORM\OneToMany(targetEntity=CategorieUnivers::class, mappedBy="univers")
     */
    private $categorieUnivers;

    public function __construct()
    {
        $this->UniversRoman = new ArrayCollection();
        $this->categorieUnivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomUnivers(): ?string
    {
        return $this->nomUnivers;
    }

    public function setNomUnivers(string $nomUnivers): self
    {
        $this->nomUnivers = $nomUnivers;

        return $this;
    }

    /**
     * @return Collection|Roman[]
     */
    public function getUniversRoman(): Collection
    {
        return $this->UniversRoman;
    }

    public function addUniversRoman(Roman $universRoman): self
    {
        if (!$this->UniversRoman->contains($universRoman)) {
            $this->UniversRoman[] = $universRoman;
            $universRoman->setUnivers($this);
        }

        return $this;
    }

    public function removeUniversRoman(Roman $universRoman): self
    {
        if ($this->UniversRoman->removeElement($universRoman)) {
            // set the owning side to null (unless already changed)
            if ($universRoman->getUnivers() === $this) {
                $universRoman->setUnivers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategorieUnivers[]
     */
    public function getCategorieUnivers(): Collection
    {
        return $this->categorieUnivers;
    }

    public function addCategorieUniver(CategorieUnivers $categorieUniver): self
    {
        if (!$this->categorieUnivers->contains($categorieUniver)) {
            $this->categorieUnivers[] = $categorieUniver;
            $categorieUniver->setUnivers($this);
        }

        return $this;
    }

    public function removeCategorieUniver(CategorieUnivers $categorieUniver): self
    {
        if ($this->categorieUnivers->removeElement($categorieUniver)) {
            // set the owning side to null (unless already changed)
            if ($categorieUniver->getUnivers() === $this) {
                $categorieUniver->setUnivers(null);
            }
        }

        return $this;
    }
}

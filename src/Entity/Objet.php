<?php

namespace App\Entity;

use App\Repository\ObjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ObjetRepository::class)
 */
class Objet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomObjet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionObjet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgObjet;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieUnivers::class, inversedBy="objets")
     */
    private $categorieUnivers;

    /**
     * @ORM\ManyToMany(targetEntity=Personnage::class, inversedBy="objets")
     */
    private $ObjetPersonnage;

    public function __construct()
    {
        $this->ObjetPersonnage = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomObjet(): ?string
    {
        return $this->nomObjet;
    }

    public function setNomObjet(string $nomObjet): self
    {
        $this->nomObjet = $nomObjet;

        return $this;
    }

    public function getDescriptionObjet(): ?string
    {
        return $this->descriptionObjet;
    }

    public function setDescriptionObjet(?string $descriptionObjet): self
    {
        $this->descriptionObjet = $descriptionObjet;

        return $this;
    }

    public function getImgObjet(): ?string
    {
        return $this->imgObjet;
    }

    public function setImgObjet(?string $imgObjet): self
    {
        $this->imgObjet = $imgObjet;

        return $this;
    }

    public function getCategorieUnivers(): ?CategorieUnivers
    {
        return $this->categorieUnivers;
    }

    public function setCategorieUnivers(?CategorieUnivers $categorieUnivers): self
    {
        $this->categorieUnivers = $categorieUnivers;

        return $this;
    }

    /**
     * @return Collection|Personnage[]
     */
    public function getObjetPersonnage(): Collection
    {
        return $this->ObjetPersonnage;
    }

    public function addObjetPersonnage(Personnage $objetPersonnage): self
    {
        if (!$this->ObjetPersonnage->contains($objetPersonnage)) {
            $this->ObjetPersonnage[] = $objetPersonnage;
        }

        return $this;
    }

    public function removeObjetPersonnage(Personnage $objetPersonnage): self
    {
        $this->ObjetPersonnage->removeElement($objetPersonnage);

        return $this;
    }
}

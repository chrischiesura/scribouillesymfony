<?php

namespace App\Entity;

use App\Repository\CategorieUniversRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieUniversRepository::class)
 */
class CategorieUnivers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomCategorieUnivers;

    /**
     * @ORM\ManyToOne(targetEntity=Univers::class, inversedBy="categorieUnivers")
     */
    private $univers;

    /**
     * @ORM\OneToMany(targetEntity=Pouvoir::class, mappedBy="categorieUnivers")
     */
    private $pouvoirs;

    /**
     * @ORM\OneToMany(targetEntity=Bestiaire::class, mappedBy="categorieUnivers")
     */
    private $bestiaires;

    /**
     * @ORM\OneToMany(targetEntity=Objet::class, mappedBy="categorieUnivers")
     */
    private $objets;

    /**
     * @ORM\OneToMany(targetEntity=Lieu::class, mappedBy="categorieUnivers")
     */
    private $lieus;

    /**
     * @ORM\OneToMany(targetEntity=Fiche::class, mappedBy="categorieUnivers")
     */
    private $fiches;

    public function __construct()
    {
        $this->pouvoirs = new ArrayCollection();
        $this->bestiaires = new ArrayCollection();
        $this->objets = new ArrayCollection();
        $this->lieus = new ArrayCollection();
        $this->fiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCategorieUnivers(): ?string
    {
        return $this->nomCategorieUnivers;
    }

    public function setNomCategorieUnivers(string $nomCategorieUnivers): self
    {
        $this->nomCategorieUnivers = $nomCategorieUnivers;

        return $this;
    }

    public function getUnivers(): ?Univers
    {
        return $this->univers;
    }

    public function setUnivers(?Univers $univers): self
    {
        $this->univers = $univers;

        return $this;
    }

    /**
     * @return Collection|Pouvoir[]
     */
    public function getPouvoirs(): Collection
    {
        return $this->pouvoirs;
    }

    public function addPouvoir(Pouvoir $pouvoir): self
    {
        if (!$this->pouvoirs->contains($pouvoir)) {
            $this->pouvoirs[] = $pouvoir;
            $pouvoir->setCategorieUnivers($this);
        }

        return $this;
    }

    public function removePouvoir(Pouvoir $pouvoir): self
    {
        if ($this->pouvoirs->removeElement($pouvoir)) {
            // set the owning side to null (unless already changed)
            if ($pouvoir->getCategorieUnivers() === $this) {
                $pouvoir->setCategorieUnivers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Bestiaire[]
     */
    public function getBestiaires(): Collection
    {
        return $this->bestiaires;
    }

    public function addBestiaire(Bestiaire $bestiaire): self
    {
        if (!$this->bestiaires->contains($bestiaire)) {
            $this->bestiaires[] = $bestiaire;
            $bestiaire->setCategorieUnivers($this);
        }

        return $this;
    }

    public function removeBestiaire(Bestiaire $bestiaire): self
    {
        if ($this->bestiaires->removeElement($bestiaire)) {
            // set the owning side to null (unless already changed)
            if ($bestiaire->getCategorieUnivers() === $this) {
                $bestiaire->setCategorieUnivers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Objet[]
     */
    public function getObjets(): Collection
    {
        return $this->objets;
    }

    public function addObjet(Objet $objet): self
    {
        if (!$this->objets->contains($objet)) {
            $this->objets[] = $objet;
            $objet->setCategorieUnivers($this);
        }

        return $this;
    }

    public function removeObjet(Objet $objet): self
    {
        if ($this->objets->removeElement($objet)) {
            // set the owning side to null (unless already changed)
            if ($objet->getCategorieUnivers() === $this) {
                $objet->setCategorieUnivers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lieu[]
     */
    public function getLieus(): Collection
    {
        return $this->lieus;
    }

    public function addLieu(Lieu $lieu): self
    {
        if (!$this->lieus->contains($lieu)) {
            $this->lieus[] = $lieu;
            $lieu->setCategorieUnivers($this);
        }

        return $this;
    }

    public function removeLieu(Lieu $lieu): self
    {
        if ($this->lieus->removeElement($lieu)) {
            // set the owning side to null (unless already changed)
            if ($lieu->getCategorieUnivers() === $this) {
                $lieu->setCategorieUnivers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Fiche[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiche $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setCategorieUnivers($this);
        }

        return $this;
    }

    public function removeFich(Fiche $fich): self
    {
        if ($this->fiches->removeElement($fich)) {
            // set the owning side to null (unless already changed)
            if ($fich->getCategorieUnivers() === $this) {
                $fich->setCategorieUnivers(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\PlanChapitreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanChapitreRepository::class)
 */
class PlanChapitre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titrePlanChapitre;

    /**
     * @ORM\Column(type="text")
     */
    private $planChapitre;

    /**
     * @ORM\ManyToOne(targetEntity=Roman::class, inversedBy="planChapitres")
     */
    private $roman;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitrePlanChapitre(): ?string
    {
        return $this->titrePlanChapitre;
    }

    public function setTitrePlanChapitre(string $titrePlanChapitre): self
    {
        $this->titrePlanChapitre = $titrePlanChapitre;

        return $this;
    }

    public function getPlanChapitre(): ?string
    {
        return $this->planChapitre;
    }

    public function setPlanChapitre(string $planChapitre): self
    {
        $this->planChapitre = $planChapitre;

        return $this;
    }

    public function getRoman(): ?Roman
    {
        return $this->roman;
    }

    public function setRoman(?Roman $roman): self
    {
        $this->roman = $roman;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\PouvoirRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PouvoirRepository::class)
 */
class Pouvoir
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomPouvoir;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionPouvoir;

    /**
     * @ORM\ManyToMany(targetEntity=Personnage::class, mappedBy="PersonnagePouvoir")
     * @ORM\ManyToMany(targetEntity="Personnage::class", mappedBy="Pouvoir", cascade={"all"}, orphanRemoval=true)

     */
    private $PouvoirPersonnage;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieUnivers::class, inversedBy="pouvoirs")
     */
    private $categorieUnivers;

    /**
     * @ORM\ManyToMany(targetEntity=Bestiaire::class, mappedBy="PouvoirBestiaire")
     */
    private $bestiaires;

    public function __construct()
    {
        $this->PouvoirPersonnage = new ArrayCollection();
        $this->bestiaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPouvoir(): ?string
    {
        return $this->nomPouvoir;
    }

    public function setNomPouvoir(?string $nomPouvoir): self
    {
        $this->nomPouvoir = $nomPouvoir;

        return $this;
    }

    public function getDescriptionPouvoir(): ?string
    {
        return $this->descriptionPouvoir;
    }

    public function setDescriptionPouvoir(?string $descriptionPouvoir): self
    {
        $this->descriptionPouvoir = $descriptionPouvoir;

        return $this;
    }

    /**
     * @return Collection|Personnage[]
     */
    public function getPouvoirPersonnage(): Collection
    {
        return $this->PouvoirPersonnage;
    }

    public function addPouvoirPersonnage(Personnage $pouvoirPersonnage): self
    {
        if (!$this->PouvoirPersonnage->contains($pouvoirPersonnage)) {
            $this->PouvoirPersonnage[] = $pouvoirPersonnage;
            $pouvoirPersonnage->addPersonnagePouvoir($this);
        }

        return $this;
    }

    public function removePouvoirPersonnage(Personnage $pouvoirPersonnage): self
    {
        if ($this->PouvoirPersonnage->removeElement($pouvoirPersonnage)) {
            $pouvoirPersonnage->removePersonnagePouvoir($this);
        }

        return $this;
    }

    public function __toString(): string {
        return $this->nomPouvoir;
    }

    public function getCategorieUnivers(): ?CategorieUnivers
    {
        return $this->categorieUnivers;
    }

    public function setCategorieUnivers(?CategorieUnivers $categorieUnivers): self
    {
        $this->categorieUnivers = $categorieUnivers;

        return $this;
    }

    /**
     * @return Collection|Bestiaire[]
     */
    public function getBestiaires(): Collection
    {
        return $this->bestiaires;
    }

    public function addBestiaire(Bestiaire $bestiaire): self
    {
        if (!$this->bestiaires->contains($bestiaire)) {
            $this->bestiaires[] = $bestiaire;
            $bestiaire->addPouvoirBestiaire($this);
        }

        return $this;
    }

    public function removeBestiaire(Bestiaire $bestiaire): self
    {
        if ($this->bestiaires->removeElement($bestiaire)) {
            $bestiaire->removePouvoirBestiaire($this);
        }

        return $this;
    }

    
}

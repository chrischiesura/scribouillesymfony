<?php

namespace App\Entity;

use App\Repository\PersonnageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonnageRepository::class)
 */
class Personnage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Pouvoir::class, inversedBy="PouvoirPersonnage")
     * @ORM\JoinTable(name="personnage_pouvoir")
     */
    private $PersonnagePouvoir;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surnomPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dateNaissancePersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ageReelPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ageApparencePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $connaitSurnaturelPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $racePersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alignementPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $histoirePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $objectifPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statutSocialPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $orientationSexuellePersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $platPrefPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allergiePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $croyancePersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $animauxPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enAmourPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enFamillePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enSociete;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $auTravailPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enAmitiePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $adversitePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $loisirPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $musiquePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $transformationPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quiTransformation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $physiqueYeuxPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $physiqueCheveuxPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $physiqueVisagePersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $physiqueTatouagePersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $physiqueCorpulence;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $physiqueCicatricesPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $physiqueDistinctionPersonnage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $physiqueVoixPersonnage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $statutMaritalPersonnage;

    /**
     * @ORM\ManyToMany(targetEntity=Bestiaire::class, mappedBy="BestiairePersonnage")
     */
    private $bestiaires;

    /**
     * @ORM\ManyToMany(targetEntity=Objet::class, mappedBy="ObjetPersonnage")
     */
    private $objets;

    /**
     * @ORM\ManyToMany(targetEntity=Lieu::class, mappedBy="LieuPersonnage")
     */
    private $lieus;

    /**
     * @ORM\ManyToMany(targetEntity=Roman::class, mappedBy="romanPersonnage")
     */
    private $roman;

    public function __construct()
    {
        $this->PersonnagePouvoir = new ArrayCollection();
        $this->bestiaires = new ArrayCollection();
        $this->objets = new ArrayCollection();
        $this->lieus = new ArrayCollection();
        $this->roman = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Pouvoir[]
     */
    public function getPersonnagePouvoir(): Collection
    {
        return $this->PersonnagePouvoir;
    }

    public function addPersonnagePouvoir(Pouvoir $personnagePouvoir): self
    {
        if (!$this->PersonnagePouvoir->contains($personnagePouvoir)) {
            $this->PersonnagePouvoir[] = $personnagePouvoir;
        }

        return $this;
    }

    public function removePersonnagePouvoir(Pouvoir $personnagePouvoir): self
    {
        $this->PersonnagePouvoir->removeElement($personnagePouvoir);

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSurnomPersonnage(): ?string
    {
        return $this->surnomPersonnage;
    }

    public function setSurnomPersonnage(?string $surnomPersonnage): self
    {
        $this->surnomPersonnage = $surnomPersonnage;

        return $this;
    }

    public function getDateNaissancePersonnage(): ?string
    {
        return $this->dateNaissancePersonnage;
    }

    public function setDateNaissancePersonnage(?string $dateNaissancePersonnage): self
    {
        $this->dateNaissancePersonnage = $dateNaissancePersonnage;

        return $this;
    }

    public function getAgeReelPersonnage(): ?string
    {
        return $this->ageReelPersonnage;
    }

    public function setAgeReelPersonnage(?string $ageReelPersonnage): self
    {
        $this->ageReelPersonnage = $ageReelPersonnage;

        return $this;
    }

    public function getAgeApparencePersonnage(): ?string
    {
        return $this->ageApparencePersonnage;
    }

    public function setAgeApparencePersonnage(?string $ageApparencePersonnage): self
    {
        $this->ageApparencePersonnage = $ageApparencePersonnage;

        return $this;
    }

    public function getConnaitSurnaturelPersonnage(): ?string
    {
        return $this->connaitSurnaturelPersonnage;
    }

    public function setConnaitSurnaturelPersonnage(?string $connaitSurnaturelPersonnage): self
    {
        $this->connaitSurnaturelPersonnage = $connaitSurnaturelPersonnage;

        return $this;
    }

    public function getRacePersonnage(): ?string
    {
        return $this->racePersonnage;
    }

    public function setRacePersonnage(?string $racePersonnage): self
    {
        $this->racePersonnage = $racePersonnage;

        return $this;
    }

    public function getAlignementPersonnage(): ?string
    {
        return $this->alignementPersonnage;
    }

    public function setAlignementPersonnage(?string $alignementPersonnage): self
    {
        $this->alignementPersonnage = $alignementPersonnage;

        return $this;
    }

    public function getHistoirePersonnage(): ?string
    {
        return $this->histoirePersonnage;
    }

    public function setHistoirePersonnage(?string $histoirePersonnage): self
    {
        $this->histoirePersonnage = $histoirePersonnage;

        return $this;
    }

    public function getObjectifPersonnage(): ?string
    {
        return $this->objectifPersonnage;
    }

    public function setObjectifPersonnage(?string $objectifPersonnage): self
    {
        $this->objectifPersonnage = $objectifPersonnage;

        return $this;
    }

    public function getStatutSocialPersonnage(): ?string
    {
        return $this->statutSocialPersonnage;
    }

    public function setStatutSocialPersonnage(?string $statutSocialPersonnage): self
    {
        $this->statutSocialPersonnage = $statutSocialPersonnage;

        return $this;
    }

    public function getOrientationSexuellePersonnage(): ?string
    {
        return $this->orientationSexuellePersonnage;
    }

    public function setOrientationSexuellePersonnage(string $orientationSexuellePersonnage): self
    {
        $this->orientationSexuellePersonnage = $orientationSexuellePersonnage;

        return $this;
    }

    public function getPlatPrefPersonnage(): ?string
    {
        return $this->platPrefPersonnage;
    }

    public function setPlatPrefPersonnage(string $platPrefPersonnage): self
    {
        $this->platPrefPersonnage = $platPrefPersonnage;

        return $this;
    }

    public function getAllergiePersonnage(): ?string
    {
        return $this->allergiePersonnage;
    }

    public function setAllergiePersonnage(?string $allergiePersonnage): self
    {
        $this->allergiePersonnage = $allergiePersonnage;

        return $this;
    }

    public function getCroyancePersonnage(): ?string
    {
        return $this->croyancePersonnage;
    }

    public function setCroyancePersonnage(?string $croyancePersonnage): self
    {
        $this->croyancePersonnage = $croyancePersonnage;

        return $this;
    }

    public function getAnimauxPersonnage(): ?string
    {
        return $this->animauxPersonnage;
    }

    public function setAnimauxPersonnage(?string $animauxPersonnage): self
    {
        $this->animauxPersonnage = $animauxPersonnage;

        return $this;
    }

    public function getEnAmourPersonnage(): ?string
    {
        return $this->enAmourPersonnage;
    }

    public function setEnAmourPersonnage(?string $enAmourPersonnage): self
    {
        $this->enAmourPersonnage = $enAmourPersonnage;

        return $this;
    }

    public function getEnFamillePersonnage(): ?string
    {
        return $this->enFamillePersonnage;
    }

    public function setEnFamillePersonnage(?string $enFamillePersonnage): self
    {
        $this->enFamillePersonnage = $enFamillePersonnage;

        return $this;
    }

    public function getEnSociete(): ?string
    {
        return $this->enSociete;
    }

    public function setEnSociete(?string $enSociete): self
    {
        $this->enSociete = $enSociete;

        return $this;
    }

    public function getAuTravailPersonnage(): ?string
    {
        return $this->auTravailPersonnage;
    }

    public function setAuTravailPersonnage(?string $auTravailPersonnage): self
    {
        $this->auTravailPersonnage = $auTravailPersonnage;

        return $this;
    }

    public function getEnAmitiePersonnage(): ?string
    {
        return $this->enAmitiePersonnage;
    }

    public function setEnAmitiePersonnage(?string $enAmitiePersonnage): self
    {
        $this->enAmitiePersonnage = $enAmitiePersonnage;

        return $this;
    }

    public function getAdversitePersonnage(): ?string
    {
        return $this->adversitePersonnage;
    }

    public function setAdversitePersonnage(?string $adversitePersonnage): self
    {
        $this->adversitePersonnage = $adversitePersonnage;

        return $this;
    }

    public function getLoisirPersonnage(): ?string
    {
        return $this->loisirPersonnage;
    }

    public function setLoisirPersonnage(?string $loisirPersonnage): self
    {
        $this->loisirPersonnage = $loisirPersonnage;

        return $this;
    }

    public function getMusiquePersonnage(): ?string
    {
        return $this->musiquePersonnage;
    }

    public function setMusiquePersonnage(?string $musiquePersonnage): self
    {
        $this->musiquePersonnage = $musiquePersonnage;

        return $this;
    }

    public function getTransformationPersonnage(): ?string
    {
        return $this->transformationPersonnage;
    }

    public function setTransformationPersonnage(string $transformationPersonnage): self
    {
        $this->transformationPersonnage = $transformationPersonnage;

        return $this;
    }

    public function getQuiTransformation(): ?string
    {
        return $this->quiTransformation;
    }

    public function setQuiTransformation(?string $quiTransformation): self
    {
        $this->quiTransformation = $quiTransformation;

        return $this;
    }

    public function getImgPersonnage(): ?string
    {
        return $this->imgPersonnage;
    }

    public function setImgPersonnage(?string $imgPersonnage): self
    {
        $this->imgPersonnage = $imgPersonnage;

        return $this;
    }

    public function getPhysiqueYeuxPersonnage(): ?string
    {
        return $this->physiqueYeuxPersonnage;
    }

    public function setPhysiqueYeuxPersonnage(?string $physiqueYeuxPersonnage): self
    {
        $this->physiqueYeuxPersonnage = $physiqueYeuxPersonnage;

        return $this;
    }

    public function getPhysiqueCheveuxPersonnage(): ?string
    {
        return $this->physiqueCheveuxPersonnage;
    }

    public function setPhysiqueCheveuxPersonnage(?string $physiqueCheveuxPersonnage): self
    {
        $this->physiqueCheveuxPersonnage = $physiqueCheveuxPersonnage;

        return $this;
    }

    public function getPhysiqueVisagePersonnage(): ?string
    {
        return $this->physiqueVisagePersonnage;
    }

    public function setPhysiqueVisagePersonnage(?string $physiqueVisagePersonnage): self
    {
        $this->physiqueVisagePersonnage = $physiqueVisagePersonnage;

        return $this;
    }

    public function getPhysiqueTatouagePersonnage(): ?string
    {
        return $this->physiqueTatouagePersonnage;
    }

    public function setPhysiqueTatouagePersonnage(?string $physiqueTatouagePersonnage): self
    {
        $this->physiqueTatouagePersonnage = $physiqueTatouagePersonnage;

        return $this;
    }

    public function getPhysiqueCorpulence(): ?string
    {
        return $this->physiqueCorpulence;
    }

    public function setPhysiqueCorpulence(?string $physiqueCorpulence): self
    {
        $this->physiqueCorpulence = $physiqueCorpulence;

        return $this;
    }

    public function getPhysiqueCicatricesPersonnage(): ?string
    {
        return $this->physiqueCicatricesPersonnage;
    }

    public function setPhysiqueCicatricesPersonnage(?string $physiqueCicatricesPersonnage): self
    {
        $this->physiqueCicatricesPersonnage = $physiqueCicatricesPersonnage;

        return $this;
    }

    public function getPhysiqueDistinctionPersonnage(): ?string
    {
        return $this->physiqueDistinctionPersonnage;
    }

    public function setPhysiqueDistinctionPersonnage(?string $physiqueDistinctionPersonnage): self
    {
        $this->physiqueDistinctionPersonnage = $physiqueDistinctionPersonnage;

        return $this;
    }

    public function getPhysiqueVoixPersonnage(): ?string
    {
        return $this->physiqueVoixPersonnage;
    }

    public function setPhysiqueVoixPersonnage(?string $physiqueVoixPersonnage): self
    {
        $this->physiqueVoixPersonnage = $physiqueVoixPersonnage;

        return $this;
    }

    public function getStatutMaritalPersonnage(): ?string
    {
        return $this->statutMaritalPersonnage;
    }

    public function setStatutMaritalPersonnage(?string $statutMaritalPersonnage): self
    {
        $this->statutMaritalPersonnage = $statutMaritalPersonnage;

        return $this;
    }

    /**
     * @return Collection|Bestiaire[]
     */
    public function getBestiaires(): Collection
    {
        return $this->bestiaires;
    }

    public function addBestiaire(Bestiaire $bestiaire): self
    {
        if (!$this->bestiaires->contains($bestiaire)) {
            $this->bestiaires[] = $bestiaire;
            $bestiaire->addBestiairePersonnage($this);
        }

        return $this;
    }

    public function removeBestiaire(Bestiaire $bestiaire): self
    {
        if ($this->bestiaires->removeElement($bestiaire)) {
            $bestiaire->removeBestiairePersonnage($this);
        }

        return $this;
    }

    /**
     * @return Collection|Objet[]
     */
    public function getObjets(): Collection
    {
        return $this->objets;
    }

    public function addObjet(Objet $objet): self
    {
        if (!$this->objets->contains($objet)) {
            $this->objets[] = $objet;
            $objet->addObjetPersonnage($this);
        }

        return $this;
    }

    public function removeObjet(Objet $objet): self
    {
        if ($this->objets->removeElement($objet)) {
            $objet->removeObjetPersonnage($this);
        }

        return $this;
    }

    /**
     * @return Collection|Lieu[]
     */
    public function getLieus(): Collection
    {
        return $this->lieus;
    }

    public function addLieu(Lieu $lieu): self
    {
        if (!$this->lieus->contains($lieu)) {
            $this->lieus[] = $lieu;
            $lieu->addLieuPersonnage($this);
        }

        return $this;
    }

    public function removeLieu(Lieu $lieu): self
    {
        if ($this->lieus->removeElement($lieu)) {
            $lieu->removeLieuPersonnage($this);
        }

        return $this;
    }

    /**
     * @return Collection|Roman[]
     */
    public function getRoman(): Collection
    {
        return $this->roman;
    }

    public function addRoman(Roman $roman): self
    {
        if (!$this->roman->contains($roman)) {
            $this->roman[] = $roman;
            $roman->addRomanPersonnage($this);
        }

        return $this;
    }

    public function removeRoman(Roman $roman): self
    {
        if ($this->roman->removeElement($roman)) {
            $roman->removeRomanPersonnage($this);
        }

        return $this;
    }

    public function __toString(): string {
        return $this->prenom;
    }
}

<?php

namespace App\Entity;

use App\Repository\RomanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RomanRepository::class)
 */
class Roman
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreRoman;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionRoman;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $genreRoman;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statutRoman;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $creationRoman;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageRoman;

    /**
     * @ORM\ManyToOne(targetEntity=Univers::class, inversedBy="UniversRoman")
     */
    private $univers;

    /**
     * @ORM\OneToMany(targetEntity=PlanChapitre::class, mappedBy="roman")
     */
    private $planChapitres;

    /**
     * @ORM\OneToMany(targetEntity=Texte::class, mappedBy="roman")
     */
    private $textes;

    /**
     * @ORM\ManyToOne(targetEntity=Auteur::class, inversedBy="auteurRoman")
     */
    private $auteur;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteRoman;

    /**
     * @ORM\ManyToMany(targetEntity=Personnage::class, inversedBy="roman")
     */
    private $romanPersonnage;

    public function __construct()
    {
        $this->planChapitres = new ArrayCollection();
        $this->textes = new ArrayCollection();
        $this->romanPersonnage = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreRoman(): ?string
    {
        return $this->titreRoman;
    }

    public function setTitreRoman(string $titreRoman): self
    {
        $this->titreRoman = $titreRoman;

        return $this;
    }

    public function getDescriptionRoman(): ?string
    {
        return $this->descriptionRoman;
    }

    public function setDescriptionRoman(?string $descriptionRoman): self
    {
        $this->descriptionRoman = $descriptionRoman;

        return $this;
    }

    public function getGenreRoman(): ?string
    {
        return $this->genreRoman;
    }

    public function setGenreRoman(?string $genreRoman): self
    {
        $this->genreRoman = $genreRoman;

        return $this;
    }

    public function getStatutRoman(): ?string
    {
        return $this->statutRoman;
    }

    public function setStatutRoman(string $statutRoman): self
    {
        $this->statutRoman = $statutRoman;

        return $this;
    }

    public function getCreationRoman(): ?\DateTimeInterface
    {
        return $this->creationRoman;
    }

    public function setCreationRoman(?\DateTimeInterface $creationRoman): self
    {
        $this->creationRoman = $creationRoman;

        return $this;
    }

    public function getImageRoman(): ?string
    {
        return $this->imageRoman;
    }

    public function setImageRoman(?string $imageRoman): self
    {
        $this->imageRoman = $imageRoman;

        return $this;
    }

    public function getUnivers(): ?Univers
    {
        return $this->univers;
    }

    public function setUnivers(?Univers $univers): self
    {
        $this->univers = $univers;

        return $this;
    }

    /**
     * @return Collection|PlanChapitre[]
     */
    public function getPlanChapitres(): Collection
    {
        return $this->planChapitres;
    }

    public function addPlanChapitre(PlanChapitre $planChapitre): self
    {
        if (!$this->planChapitres->contains($planChapitre)) {
            $this->planChapitres[] = $planChapitre;
            $planChapitre->setRoman($this);
        }

        return $this;
    }

    public function removePlanChapitre(PlanChapitre $planChapitre): self
    {
        if ($this->planChapitres->removeElement($planChapitre)) {
            // set the owning side to null (unless already changed)
            if ($planChapitre->getRoman() === $this) {
                $planChapitre->setRoman(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Texte[]
     */
    public function getTextes(): Collection
    {
        return $this->textes;
    }

    public function addTexte(Texte $texte): self
    {
        if (!$this->textes->contains($texte)) {
            $this->textes[] = $texte;
            $texte->setRoman($this);
        }

        return $this;
    }

    public function removeTexte(Texte $texte): self
    {
        if ($this->textes->removeElement($texte)) {
            // set the owning side to null (unless already changed)
            if ($texte->getRoman() === $this) {
                $texte->setRoman(null);
            }
        }

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getTexteRoman(): ?string
    {
        return $this->texteRoman;
    }

    public function setTexteRoman(?string $texteRoman): self
    {
        $this->texteRoman = $texteRoman;

        return $this;
    }

    /**
     * @return Collection|Personnage[]
     */
    public function getRomanPersonnage(): Collection
    {
        return $this->romanPersonnage;
    }

    public function addRomanPersonnage(Personnage $romanPersonnage): self
    {
        if (!$this->romanPersonnage->contains($romanPersonnage)) {
            $this->romanPersonnage[] = $romanPersonnage;
        }

        return $this;
    }

    public function removeRomanPersonnage(Personnage $romanPersonnage): self
    {
        $this->romanPersonnage->removeElement($romanPersonnage);

        return $this;
    }


}

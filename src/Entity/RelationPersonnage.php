<?php

namespace App\Entity;

use App\Repository\RelationPersonnageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RelationPersonnageRepository::class)
 */
class RelationPersonnage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $natureLien;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionLien;

    /**
     * @ORM\OneToOne(targetEntity=Personnage::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnage1;

    /**
     * @ORM\OneToOne(targetEntity=Personnage::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnage2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNatureLien(): ?string
    {
        return $this->natureLien;
    }

    public function setNatureLien(?string $natureLien): self
    {
        $this->natureLien = $natureLien;

        return $this;
    }

    public function getDescriptionLien(): ?string
    {
        return $this->descriptionLien;
    }

    public function setDescriptionLien(?string $descriptionLien): self
    {
        $this->descriptionLien = $descriptionLien;

        return $this;
    }

    public function getPersonnage1(): ?Personnage
    {
        return $this->personnage1;
    }

    public function setPersonnage1(Personnage $personnage1): self
    {
        $this->personnage1 = $personnage1;

        return $this;
    }

    public function getPersonnage2(): ?Personnage
    {
        return $this->personnage2;
    }

    public function setPersonnage2(Personnage $personnage2): self
    {
        $this->personnage2 = $personnage2;

        return $this;
    }
}

<?php

namespace App\Controller;

use App\Entity\FicheBestiaire;
use App\Form\BestiaireFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BestiaireController extends AbstractController
{
    /**
     * @Route("/bestiaire/nouveau", name="app_bestiaire_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $bestiaire = new FicheBestiaire();
        $formBestiaire = $this->createForm(BestiaireFormType::class, $bestiaire);
        $formBestiaire->handleRequest($request);

        if($formBestiaire->isSubmitted()){
            $manager->persist($bestiaire);
            $manager->flush();
        }
            // do anything else you need here, like send an email

            return $this->render('Bestiaire/nouveau.html.twig', [
            'form' => $formBestiaire->createView(),
        ]);
        }

       
    }


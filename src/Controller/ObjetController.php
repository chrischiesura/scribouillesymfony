<?php

namespace App\Controller;

use App\Entity\FicheObjet;
use App\Form\ObjetFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ObjetController extends AbstractController
{
    /**
     * @Route("/objet/nouveau", name="app_objet_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $objet = new FicheObjet();
        $formObjet = $this->createForm(ObjetFormType::class, $objet);
        $formObjet->handleRequest($request);

        if($formObjet->isSubmitted()){
            $manager->persist($objet);
            $manager->flush();
        }
            // do anything else you need here, like send an email

            return $this->render('Objet/nouveau.html.twig', [
            'form' => $formObjet->createView(),
        ]);
        }

       
    }


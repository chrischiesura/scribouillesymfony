<?php

namespace App\Controller;

use App\Entity\PlanChapitre;
use App\Entity\Roman;
use App\Form\RomanCreerModifierFormType;
use App\Form\RomanEcrireFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;


class RomanController extends AbstractController
{
    /**
     * @Route("/roman/nouveau", name="app_roman_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager, SluggerInterface $slugger): Response
    {
        $roman = new Roman();
        $form = $this->createForm(RomanCreerModifierFormType::class, $roman);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $ImageRomanFile = $form->get('image_roman')->getData();
            if($ImageRomanFile){
                $originalFilename = pathinfo($ImageRomanFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$ImageRomanFile->guessExtension();
            }

                    try{
                        $ImageRomanFile->move(
                            $this->getParameter('images_directory'),
                            $newFilename
                        );
                    } catch(FileException $e){
                        // mettre handle exception si quelque chose arrive
                    }
                    $roman->setImageRoman($newFilename);  
                    $manager->persist($roman);
                    $manager->flush();

                    return $this->redirectToRoute('roman_show');
        }
            // do anything else you need here, like send an email
            return $this->render('Roman/nouveau.html.twig', [
            'form' => $form->createView(),
        ]);
        }


    /**
     * @Route("/roman/{id}", name="roman_show")
     */
    public function show(string $id, Request $request, Roman $writeroman,EntityManagerInterface $manager): Response
    {
        $roman = $this->getDoctrine()
            ->getRepository(Roman::class)
            ->find($id);

            $planChapitre = $this->getDoctrine()
            ->getRepository(PlanChapitre::class)
            ->findBy(['roman' => $roman,]);



        if (!$roman){
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }
        
        $form = $this->createForm(RomanEcrireFormType::class, $writeroman);
        $form->handleRequest($request);

        if($form->isSubmitted()){
                    $manager->persist($writeroman);
                    $manager->flush();

                    
        }

        return $this->render('Roman/roman.html.twig',
    ['roman' => $roman,
    'planChapitre'=> $planChapitre,
    'form'=>$form->createView()]);
    }
}
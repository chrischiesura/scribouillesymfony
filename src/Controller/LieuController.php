<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Form\LieuFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LieuController extends AbstractController
{
    /**
     * @Route("/lieu/nouveau", name="app_lieu_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $lieu = new Lieu();
        $formLieu = $this->createForm(LieuFormType::class, $lieu);
        $formLieu->handleRequest($request);

        if($formLieu->isSubmitted()){
            $manager->persist($lieu);
            $manager->flush();
        }
            // do anything else you need here, like send an email

            return $this->render('Lieu/nouveau.html.twig', [
            'form' => $formLieu->createView(),
        ]);
        }

       
    }


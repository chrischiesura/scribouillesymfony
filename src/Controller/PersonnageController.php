<?php

namespace App\Controller;

use App\Entity\Personnage;
use App\Entity\Pouvoir;
use App\Form\PersonnageFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonnageController extends AbstractController
{


    /**
     * @Route("/personnage/nouveau", name="app_personnage_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $personnage = new Personnage();
        $form = $this->createForm(PersonnageFormType::class, $personnage);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $manager->persist($personnage);
            $manager->flush();
        }
        
            return $this->render('Personnage/nouveau.html.twig', [
            'form' => $form->createView(),
        ]);
        }
/**
     * @Route("/personnage/{id}", name="personnage_show")
     */
    public function show(int $id): Response
    {
        $personnage = $this->getDoctrine()
            ->getRepository(Personnage::class)
            ->find($id);

    return $this->render('Personnage/personnage.html.twig', [
        'personnage' => $personnage,
        ]);
        }
        

    }


<?php

namespace App\Controller;

use App\Entity\Pouvoir;
use App\Form\PouvoirFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PouvoirController extends AbstractController
{
    /**
     * @Route("/pouvoir/nouveau", name="app_pouvoir_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $pouvoir = new Pouvoir();
        $formPouvoir = $this->createForm(PouvoirFormType::class, $pouvoir);
        $formPouvoir->handleRequest($request);

        if($formPouvoir->isSubmitted()){
            $manager->persist($pouvoir);
            $manager->flush();
        }
            // do anything else you need here, like send an email

            return $this->render('Pouvoir/nouveau.html.twig', [
            'form' => $formPouvoir->createView(),
        ]);
        }


    }


<?php

namespace App\Controller;

use App\Entity\Univers;
use App\Form\UniversFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UniversController extends AbstractController
{
    /**
     * @Route("/univers/nouveau", name="app_univers_nouveau")
     */
    public function register(Request $request, EntityManagerInterface $manager): Response
    {
        $univers = new Univers();
        $formUnivers = $this->createForm(UniversFormType::class, $univers);
        $formUnivers->handleRequest($request);

        if($formUnivers->isSubmitted()){
            $manager->persist($univers);
            $manager->flush();
        }
            // do anything else you need here, like send an email

            return $this->render('Univers/nouveau.html.twig', [
            'form' => $formUnivers->createView(),
        ]);
        }

       
    }


<?php

namespace App\Form;

use App\Entity\FicheBestiaire;
use Doctrine\DBAL\Types\IntegerType as TypesIntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType as TypeIntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\Common\Collections\Collection;

class BestiaireFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom_bestiaire', TextType::class,[
                'required' => true,
            ])
            ->add('contenu_bestiaire', TextType::class)
            ->add('force_bestiaire' , TextType::class)
            ->add('faiblesse_bestiaire' , TextType::class)
            ->add('apparence_bestiaire' , TextType::class)
            ->add('alimentaire_bestiaire' , TextType::class)
            ->add('particularite_bestiaire' , TextType::class)
            ->add('img_bestiaire' , TextType::class)
            
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-info',
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FicheBestiaire::class,
        ]);
    }
}

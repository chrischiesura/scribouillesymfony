<?php

namespace App\Form;

use App\Entity\Roman;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\File;

class RomanCreerModifierFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre_roman', TextType::class,[
                'required' => true,
            ])

            ->add('genre_roman' , TextType::class)
            ->add('statut_roman', TextType::class)
            ->add('creation_roman', DateType::class)
            ->add('statut_roman', TextType::class)
            ->add('description_roman', CKEditorType::class)
            ->add('image_roman', FileType::class, [
                'mapped'=> false,
                'constraints'=>[
                    new File([
                        'mimeTypes'=>[
                            'image/jpg',
                            'image/png',
                            'image/jpeg'
                        ]
                    ])
                ]
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-info',
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Roman::class,
        ]);
    }
}

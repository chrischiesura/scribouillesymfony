<?php

namespace App\Form;

use App\Entity\Personnage;
use App\Entity\Pouvoir;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PersonnageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        
        $builder
            ->add('prenom')
            ->add('nom')
            ->add('surnomPersonnage')
            ->add('dateNaissancePersonnage')
            ->add('ageReelPersonnage')
            ->add('ageApparencePersonnage')
            ->add('connaitSurnaturelPersonnage')
            ->add('racePersonnage')
            ->add('PersonnagePouvoir', EntityType::class, array(
                'class' => Pouvoir::class,
                'choice_label' => 'nomPouvoir',
                'label'        => 'nomPouvoir',
                'expanded'     => true,
                'multiple'     => true,
            ))
            ->add('alignementPersonnage')
            ->add('histoirePersonnage')
            ->add('objectifPersonnage')
            ->add('statutSocialPersonnage')
            ->add('orientationSexuellePersonnage')
            ->add('platPrefPersonnage')
            ->add('allergiePersonnage')
            ->add('croyancePersonnage')
            ->add('animauxPersonnage')
            ->add('enAmourPersonnage')
            ->add('enFamillePersonnage')
            ->add('enSociete')
            ->add('auTravailPersonnage')
            ->add('enAmitiePersonnage')
            ->add('adversitePersonnage')
            ->add('loisirPersonnage')
            ->add('musiquePersonnage')
            ->add('transformationPersonnage')
            ->add('quiTransformation')
            ->add('imgPersonnage')
            ->add('physiqueYeuxPersonnage')
            ->add('physiqueCheveuxPersonnage')
            ->add('physiqueVisagePersonnage')
            ->add('physiqueTatouagePersonnage')
            ->add('physiqueCorpulence')
            ->add('physiqueCicatricesPersonnage')
            ->add('physiqueDistinctionPersonnage')
            ->add('physiqueVoixPersonnage')
            ->add('statutMaritalPersonnage')

            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-info',
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personnage::class,
        ]);
    }
}

<?php

namespace App\Repository;

use App\Entity\CategorieUnivers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieUnivers|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieUnivers|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieUnivers[]    findAll()
 * @method CategorieUnivers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieUniversRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieUnivers::class);
    }

    // /**
    //  * @return CategorieUnivers[] Returns an array of CategorieUnivers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieUnivers
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

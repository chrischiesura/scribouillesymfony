<?php

namespace App\Repository;

use App\Entity\PlanChapitre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlanChapitre|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanChapitre|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanChapitre[]    findAll()
 * @method PlanChapitre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanChapitreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanChapitre::class);
    }

    // /**
    //  * @return PlanChapitre[] Returns an array of PlanChapitre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlanChapitre
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\RelationPersonnage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RelationPersonnage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelationPersonnage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelationPersonnage[]    findAll()
 * @method RelationPersonnage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelationPersonnageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelationPersonnage::class);
    }

    // /**
    //  * @return RelationPersonnage[] Returns an array of RelationPersonnage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RelationPersonnage
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

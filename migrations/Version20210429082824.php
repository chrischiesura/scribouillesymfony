<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210429082824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roman_personnage (roman_id INT NOT NULL, personnage_id INT NOT NULL, INDEX IDX_E9CC1117A468ABE3 (roman_id), INDEX IDX_E9CC11175E315342 (personnage_id), PRIMARY KEY(roman_id, personnage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE roman_personnage ADD CONSTRAINT FK_E9CC1117A468ABE3 FOREIGN KEY (roman_id) REFERENCES roman (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roman_personnage ADD CONSTRAINT FK_E9CC11175E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE plan_chapitre DROP FOREIGN KEY FK_8207875FA468ABE3');
        $this->addSql('ALTER TABLE plan_chapitre ADD CONSTRAINT FK_8207875FA468ABE3 FOREIGN KEY (roman_id) REFERENCES roman (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE roman_personnage');
        $this->addSql('ALTER TABLE plan_chapitre DROP FOREIGN KEY FK_8207875FA468ABE3');
        $this->addSql('ALTER TABLE plan_chapitre ADD CONSTRAINT FK_8207875FA468ABE3 FOREIGN KEY (roman_id) REFERENCES roman (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }
}

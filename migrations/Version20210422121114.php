<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422121114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bestiaire_personnage (bestiaire_id INT NOT NULL, personnage_id INT NOT NULL, INDEX IDX_BE401C19F57AA82F (bestiaire_id), INDEX IDX_BE401C195E315342 (personnage_id), PRIMARY KEY(bestiaire_id, personnage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bestiaire_personnage ADD CONSTRAINT FK_BE401C19F57AA82F FOREIGN KEY (bestiaire_id) REFERENCES bestiaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bestiaire_personnage ADD CONSTRAINT FK_BE401C195E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bestiaire_personnage');
    }
}

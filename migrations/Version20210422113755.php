<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422113755 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE texte ADD roman_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE texte ADD CONSTRAINT FK_EAE1A6EEA468ABE3 FOREIGN KEY (roman_id) REFERENCES roman (id)');
        $this->addSql('CREATE INDEX IDX_EAE1A6EEA468ABE3 ON texte (roman_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE texte DROP FOREIGN KEY FK_EAE1A6EEA468ABE3');
        $this->addSql('DROP INDEX IDX_EAE1A6EEA468ABE3 ON texte');
        $this->addSql('ALTER TABLE texte DROP roman_id');
    }
}

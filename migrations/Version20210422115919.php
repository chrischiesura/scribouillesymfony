<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422115919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bestiaire ADD categorie_univers_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bestiaire ADD CONSTRAINT FK_31CB6D64823FD6A7 FOREIGN KEY (categorie_univers_id) REFERENCES categorie_univers (id)');
        $this->addSql('CREATE INDEX IDX_31CB6D64823FD6A7 ON bestiaire (categorie_univers_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bestiaire DROP FOREIGN KEY FK_31CB6D64823FD6A7');
        $this->addSql('DROP INDEX IDX_31CB6D64823FD6A7 ON bestiaire');
        $this->addSql('ALTER TABLE bestiaire DROP categorie_univers_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422120824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bestiaire_pouvoir (bestiaire_id INT NOT NULL, pouvoir_id INT NOT NULL, INDEX IDX_9399739EF57AA82F (bestiaire_id), INDEX IDX_9399739EC8A705F8 (pouvoir_id), PRIMARY KEY(bestiaire_id, pouvoir_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bestiaire_pouvoir ADD CONSTRAINT FK_9399739EF57AA82F FOREIGN KEY (bestiaire_id) REFERENCES bestiaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bestiaire_pouvoir ADD CONSTRAINT FK_9399739EC8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bestiaire_pouvoir');
    }
}

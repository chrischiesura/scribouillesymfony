<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416072418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage ADD prenom VARCHAR(255) DEFAULT NULL, ADD nom VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C75E315342');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C7C8A705F8');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C75E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C7C8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage DROP prenom, DROP nom');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C75E315342');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C7C8A705F8');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C75E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C7C8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }
}

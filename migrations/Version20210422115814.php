<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422115814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bestiaire (id INT AUTO_INCREMENT NOT NULL, nom_bestiaire VARCHAR(255) NOT NULL, contenu_bestiaire LONGTEXT DEFAULT NULL, force_bestiaire LONGTEXT DEFAULT NULL, faiblesse_bestiaire LONGTEXT DEFAULT NULL, apparence_bestiaire LONGTEXT DEFAULT NULL, alimentaire_bestiaire LONGTEXT DEFAULT NULL, particularite_bestiaire LONGTEXT DEFAULT NULL, img_bestiaire VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bestiaire');
    }
}

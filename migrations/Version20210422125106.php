<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422125106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation_personnage ADD personnage1_id INT NOT NULL, ADD personnage2_id INT NOT NULL');
        $this->addSql('ALTER TABLE relation_personnage ADD CONSTRAINT FK_80FA92744EE03C7C FOREIGN KEY (personnage1_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE relation_personnage ADD CONSTRAINT FK_80FA92745C559392 FOREIGN KEY (personnage2_id) REFERENCES personnage (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_80FA92744EE03C7C ON relation_personnage (personnage1_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_80FA92745C559392 ON relation_personnage (personnage2_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation_personnage DROP FOREIGN KEY FK_80FA92744EE03C7C');
        $this->addSql('ALTER TABLE relation_personnage DROP FOREIGN KEY FK_80FA92745C559392');
        $this->addSql('DROP INDEX UNIQ_80FA92744EE03C7C ON relation_personnage');
        $this->addSql('DROP INDEX UNIQ_80FA92745C559392 ON relation_personnage');
        $this->addSql('ALTER TABLE relation_personnage DROP personnage1_id, DROP personnage2_id');
    }
}

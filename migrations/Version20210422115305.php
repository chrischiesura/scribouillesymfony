<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422115305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pouvoir ADD categorie_univers_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pouvoir ADD CONSTRAINT FK_BE7F6EC6823FD6A7 FOREIGN KEY (categorie_univers_id) REFERENCES categorie_univers (id)');
        $this->addSql('CREATE INDEX IDX_BE7F6EC6823FD6A7 ON pouvoir (categorie_univers_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pouvoir DROP FOREIGN KEY FK_BE7F6EC6823FD6A7');
        $this->addSql('DROP INDEX IDX_BE7F6EC6823FD6A7 ON pouvoir');
        $this->addSql('ALTER TABLE pouvoir DROP categorie_univers_id');
    }
}

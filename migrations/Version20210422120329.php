<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422120329 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE lieu (id INT AUTO_INCREMENT NOT NULL, categorie_univers_id INT DEFAULT NULL, nom_lieu VARCHAR(255) DEFAULT NULL, description_lieu LONGTEXT DEFAULT NULL, type_lieu VARCHAR(255) DEFAULT NULL, img_lieu VARCHAR(255) DEFAULT NULL, INDEX IDX_2F577D59823FD6A7 (categorie_univers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lieu ADD CONSTRAINT FK_2F577D59823FD6A7 FOREIGN KEY (categorie_univers_id) REFERENCES categorie_univers (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE lieu');
    }
}

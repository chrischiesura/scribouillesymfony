<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422134126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roman ADD auteur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roman ADD CONSTRAINT FK_B096ED2D60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES auteur (id)');
        $this->addSql('CREATE INDEX IDX_B096ED2D60BB6FE6 ON roman (auteur_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roman DROP FOREIGN KEY FK_B096ED2D60BB6FE6');
        $this->addSql('DROP INDEX IDX_B096ED2D60BB6FE6 ON roman');
        $this->addSql('ALTER TABLE roman DROP auteur_id');
    }
}

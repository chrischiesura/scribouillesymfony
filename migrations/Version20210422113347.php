<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422113347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plan_chapitre ADD roman_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan_chapitre ADD CONSTRAINT FK_8207875FA468ABE3 FOREIGN KEY (roman_id) REFERENCES roman (id)');
        $this->addSql('CREATE INDEX IDX_8207875FA468ABE3 ON plan_chapitre (roman_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plan_chapitre DROP FOREIGN KEY FK_8207875FA468ABE3');
        $this->addSql('DROP INDEX IDX_8207875FA468ABE3 ON plan_chapitre');
        $this->addSql('ALTER TABLE plan_chapitre DROP roman_id');
    }
}

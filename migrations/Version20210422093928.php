<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422093928 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage ADD surnom_personnage VARCHAR(255) DEFAULT NULL, ADD date_naissance_personnage VARCHAR(255) DEFAULT NULL, ADD age_reel_personnage VARCHAR(255) DEFAULT NULL, ADD age_apparence_personnage VARCHAR(255) DEFAULT NULL, ADD connait_surnaturel_personnage LONGTEXT DEFAULT NULL, ADD race_personnage VARCHAR(255) DEFAULT NULL, ADD alignement_personnage VARCHAR(255) DEFAULT NULL, ADD histoire_personnage LONGTEXT DEFAULT NULL, ADD objectif_personnage LONGTEXT DEFAULT NULL, ADD statut_social_personnage VARCHAR(255) DEFAULT NULL, ADD orientation_sexuelle_personnage VARCHAR(255) NOT NULL, ADD plat_pref_personnage VARCHAR(255) NOT NULL, ADD allergie_personnage VARCHAR(255) DEFAULT NULL, ADD croyance_personnage LONGTEXT DEFAULT NULL, ADD animaux_personnage VARCHAR(255) DEFAULT NULL, ADD en_amour_personnage LONGTEXT DEFAULT NULL, ADD en_famille_personnage LONGTEXT DEFAULT NULL, ADD en_societe LONGTEXT DEFAULT NULL, ADD au_travail_personnage LONGTEXT DEFAULT NULL, ADD en_amitie_personnage VARCHAR(255) DEFAULT NULL, ADD adversite_personnage LONGTEXT DEFAULT NULL, ADD loisir_personnage LONGTEXT DEFAULT NULL, ADD musique_personnage LONGTEXT DEFAULT NULL, ADD transformation_personnage LONGTEXT NOT NULL, ADD qui_transformation VARCHAR(255) DEFAULT NULL, ADD img_personnage VARCHAR(255) DEFAULT NULL, ADD physique_yeux_personnage VARCHAR(255) DEFAULT NULL, ADD physique_cheveux_personnage VARCHAR(255) DEFAULT NULL, ADD physique_visage_personnage VARCHAR(255) DEFAULT NULL, ADD physique_tatouage_personnage LONGTEXT DEFAULT NULL, ADD physique_corpulence VARCHAR(255) DEFAULT NULL, ADD physique_cicatrices_personnage LONGTEXT DEFAULT NULL, ADD physique_distinction_personnage LONGTEXT DEFAULT NULL, ADD physique_voix_personnage VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C75E315342');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C7C8A705F8');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C75E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C7C8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage DROP surnom_personnage, DROP date_naissance_personnage, DROP age_reel_personnage, DROP age_apparence_personnage, DROP connait_surnaturel_personnage, DROP race_personnage, DROP alignement_personnage, DROP histoire_personnage, DROP objectif_personnage, DROP statut_social_personnage, DROP orientation_sexuelle_personnage, DROP plat_pref_personnage, DROP allergie_personnage, DROP croyance_personnage, DROP animaux_personnage, DROP en_amour_personnage, DROP en_famille_personnage, DROP en_societe, DROP au_travail_personnage, DROP en_amitie_personnage, DROP adversite_personnage, DROP loisir_personnage, DROP musique_personnage, DROP transformation_personnage, DROP qui_transformation, DROP img_personnage, DROP physique_yeux_personnage, DROP physique_cheveux_personnage, DROP physique_visage_personnage, DROP physique_tatouage_personnage, DROP physique_corpulence, DROP physique_cicatrices_personnage, DROP physique_distinction_personnage, DROP physique_voix_personnage');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C75E315342');
        $this->addSql('ALTER TABLE personnage_pouvoir DROP FOREIGN KEY FK_926056C7C8A705F8');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C75E315342 FOREIGN KEY (personnage_id) REFERENCES personnage (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personnage_pouvoir ADD CONSTRAINT FK_926056C7C8A705F8 FOREIGN KEY (pouvoir_id) REFERENCES pouvoir (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422094324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage ADD statut_marital_personnage LONGTEXT DEFAULT NULL, CHANGE orientation_sexuelle_personnage orientation_sexuelle_personnage VARCHAR(255) NOT NULL, CHANGE plat_pref_personnage plat_pref_personnage VARCHAR(255) NOT NULL, CHANGE transformation_personnage transformation_personnage LONGTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personnage DROP statut_marital_personnage, CHANGE orientation_sexuelle_personnage orientation_sexuelle_personnage VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE plat_pref_personnage plat_pref_personnage VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE transformation_personnage transformation_personnage LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
